/*
 * Programmed by Muhammad Ahsan
 * muhammad.ahsan@gmail.com
 * Research Intern
 * Yahoo! Research Barcelona
 * Spain
 */
package collaborativefiltering;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.neighboursearch.LinearNNSearch;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;

/**
 *
 * @author Ahsan
 */
public class CollaborativeFiltering {

    private static Instances FilterAttributes(Instances dataset, int idx) throws Exception {

        System.out.println("Original dataset attributes = " + dataset.numAttributes());
        String[] options = new String[2];
        options[0] = "-R";                                    // "range"
        options[1] = Integer.toString(idx);                                     // first attribute
        Remove remove = new Remove();                         // new instance of filter
        remove.setOptions(options);                           // set options
        remove.setInputFormat(dataset);                          // inform filter about dataset **AFTER** setting options
        Instances newData = Filter.useFilter(dataset, remove);   // apply filter
        System.out.println("New dataset attributes = " + newData.numAttributes());
        return newData;
    }

    // Could not find implementation for row selection
    private static Instances FilterInstances(Instances dataset, String FilerString, int idx) throws Exception {
//        // Filter based on rating_final for restaurents
//        String[] options = new String[2];
//        // options[0] = "-S";
//        options[0] = "-C";
//        options[1] = FilerString;
//        System.out.println("Total Instances = " + dataset.numInstances());
//
//        // RemoveWithValues R = new RemoveWithValues();
//        SubsetByExpression R = new SubsetByExpression();
//        R.setOptions(options);
//        R.setInputFormat(dataset);
//        Instances FRDataset = Filter.useFilter(dataset, R);
//        System.out.println("Total Instances after Row Filteration = " + FRDataset.numInstances());
        return null;
    }

    public static void main(String[] args) throws Exception {
        boolean debug = true;
        String EmptyVal = "empty";
        if (args.length < 2) {
            System.out.println("Argument <ratings file name> <active user description in file <file name>>");
            System.exit(0);
        }

        Instances dataset = null;
        Instances activeUsers;
        Instance activeUser = null;

        //loop through the file and read instances
        try {
            BufferedReader bufRead = new BufferedReader(
                    new FileReader(args[0]));
            dataset = new Instances(bufRead);
            bufRead.close();

        } catch (IOException ioe) {
            System.out.println("Error reading file '" + args[0] + "' ");
            if (debug) {
                // ioe.printStackTrace();
            } else {
                System.out.println(ioe.getMessage());
            }
            System.exit(0);
        }

        try {
            BufferedReader bufRead = new BufferedReader(
                    new FileReader(args[1]));

            activeUsers = new Instances(bufRead);
            @SuppressWarnings("unchecked")
            Enumeration<Instances> en = activeUsers.enumerateInstances();
            activeUser = (Instance) en.nextElement();
            bufRead.close();

        } catch (IOException ioe) {
            System.out.println("Error reading file '" + args[1] + "' ");
            if (debug) {
                // ioe.printStackTrace();
            } else {
                System.out.println(ioe.getMessage());
            }
            System.exit(0);
        }
        // FilterInstances(dataset, "135085", 2);
        // Searcher
        // Class implementing the brute force search algorithm for nearest neighbour search

        LinearNNSearch kNN = new LinearNNSearch(dataset);
        Instances neighbors;
        double[] distances;

        try {
            neighbors = kNN.kNearestNeighbours(activeUser, 3);
            // Mandatory to call kNN.kNearestNeighbours before kNN.getDistances()
            distances = kNN.getDistances();
        } catch (Exception e) {
            System.out.println("Neighbors could not be found.");
            return;
        }

        double[] similarities = new double[distances.length];
        for (int i = 0; i < distances.length; i++) {
            System.out.println(neighbors.get(i).stringValue(0));
            similarities[i] = 1.0 / distances[i];
            if (debug) {
                System.out.println("Difference of " + (i + 1) + " = " + distances[i]
                        + " and Similarity of " + (i + 1) + " = " + similarities[i]);
            }
        }

        @SuppressWarnings("unchecked")
        Enumeration<Instances> nInstances = neighbors.enumerateInstances();
        // For the single user, all nearest neighbours are obtained

        Map<String, List<String>> recommendations = new HashMap<String, List<String>>();
        // Looping through all nearest neighbours
        while (nInstances.hasMoreElements()) {
            Instance currNeighbor = (Instance) nInstances.nextElement();

            for (int i = 0; i < currNeighbor.numAttributes(); i++) {
                if (activeUser.isMissing(i)) {
                    String attrName = activeUser.attribute(i).name();
                    // System.out.println(attrName);
                    List<String> lst = new ArrayList<String>();
                    // Check if already Map entry exists
                    if (recommendations.containsKey(attrName)) {
                        lst = recommendations.get(attrName);
                    }
                    if (!currNeighbor.isMissing(i)) {
                        lst.add(Double.toString(currNeighbor.value(i)));

                    }
                    recommendations.put(attrName, lst);
                }
            }

        }

        // Map contains Attribute Name and List of Attribute values from all nearest neighbours

        List<RecommendationRecord> finalRanks = new ArrayList<RecommendationRecord>();

        Iterator<String> it = recommendations.keySet().iterator();
        while (it.hasNext()) {
            String atrName = it.next();
            double totalImpact = 0;
            double weightedSum = 0;
            List<String> ranks = recommendations.get(atrName);
            // Ranking can be weighted average or mode depends on type of attribute
            for (int i = 0; i < ranks.size(); i++) {
                String val = ranks.get(i);
                totalImpact += similarities[i];
                weightedSum += similarities[i] * Double.parseDouble(val);

            }
            RecommendationRecord rec = new RecommendationRecord();
            rec.attributeName = atrName;
            rec.score = weightedSum / totalImpact;

            finalRanks.add(rec);
        }
        Collections.sort(finalRanks);

        // Recommendation Record contains the Attribute Name and Attribute Value
        // Print all recommendation
        for (int i = 0; i < finalRanks.size(); i++) {
            System.out.println(finalRanks.get(i));
        }

    }

    private static class RecommendationRecord implements Comparable<RecommendationRecord> {

        public double score;
        public String attributeName;

        @Override
        public int compareTo(RecommendationRecord other) {
            // Obj comparison method
            if (this.score > other.score) {
                return -1;
            }
            if (this.score < other.score) {
                return 1;
            }
            return 0;
        }

        @Override
        public String toString() {
            // Obj content display method
            return attributeName + ": " + score;
        }
    }
}
